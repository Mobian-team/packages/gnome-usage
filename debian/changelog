gnome-usage (45.0-1mobian1) unstable; urgency=medium

  * d/gbp.conf: adapt to Mobian
  * debian: import Purism UI patches.
    Reworked by Markus Göllnitz, see branch `thermal-v45` at
    https://gitlab.gnome.org/camelCaseNick/gnome-usage/

 -- Arnaud Ferraris <aferraris@debian.org>  Sat, 09 Dec 2023 15:52:09 +0100

gnome-usage (45.0-1) unstable; urgency=medium

  * New upstream release
  * Build with GTK4 & libadwaita
  * Update standards version to 4.6.2, no changes needed.

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 18 Sep 2023 07:24:01 -0400

gnome-usage (3.38.1-3) unstable; urgency=medium

  * Bump debhelper-compat to 13
  * debian/rules: Drop unneeded -Wl,--as-needed
  * Upload to unstable

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 31 Aug 2021 17:26:32 -0400

gnome-usage (3.38.1-2) experimental; urgency=medium

  * Revert changes to revert to tracker 2

 -- Iain Lane <laney@debian.org>  Thu, 24 Jun 2021 17:41:48 +0100

gnome-usage (3.38.1-1) experimental; urgency=medium

  * New upstream release

 -- Sebastien Bacher <seb128@ubuntu.com>  Tue, 23 Mar 2021 09:41:03 +0100

gnome-usage (3.38.0-1) unstable; urgency=medium

  * New upstream release
  * debian/gbp.conf: Drop 'v' prefix on upstream-tag
  * debian/patches: Revert support for tracker-3 and use tracker-2
  * debian/control: Bump build dependencies to match upstream

 -- Marco Trevisan (Treviño) <marco@ubuntu.com>  Mon, 14 Sep 2020 12:16:16 +0200

gnome-usage (3.32.0-1) unstable; urgency=medium

  * New upstream release
  * Update debian/copyright
  * Build-Depend on libdazzle-1.0-dev
  * Build-Depend on dh-sequence-gnome instead of gnome-pkg-tools
  * Bump Standards-Version to 4.4.0

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 22 Sep 2019 19:45:47 -0400

gnome-usage (3.30.0-2) unstable; urgency=medium

  * Build-Depend on debhelper-compat 12 and drop debian/compat
  * Add -Wl,-O1 to our LDFLAGS
  * Bump Standards-Version to 4.3.0

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 25 Dec 2018 17:04:41 -0500

gnome-usage (3.30.0-1) unstable; urgency=medium

  * New upstream release
  * Drop 0001-Fix-build-with-vala-0.42.patch: Applied in new release
  * Bump Standards-Version to 4.2.1

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 25 Sep 2018 10:10:35 -0400

gnome-usage (3.28.0-2) unstable; urgency=medium

  [ Jeremy Bicha ]
  * debian/watch: Only watch for stable releases

  [ Andreas Henriksson ]
  * Add 0001-Fix-build-with-vala-0.42.patch (Closes: #907934)

 -- Andreas Henriksson <andreas@fatal.se>  Sat, 08 Sep 2018 00:14:17 +0200

gnome-usage (3.28.0-1) unstable; urgency=medium

  * New upstream release
  * Drop obsolete Build-Depends on libaccountsservice-dev

 -- Jeremy Bicha <jbicha@debian.org>  Wed, 14 Mar 2018 19:28:55 -0400

gnome-usage (3.27.90-1) unstable; urgency=medium

  * Initial release (Closes: #890510)

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 16 Feb 2018 09:24:37 -0500
